from nested_inline.admin import NestedStackedInline

from ..models import Answer
from django.contrib import admin
from ..models.question import Question
from django import forms


class QuestionAdminForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = '__all__'

    def clean(self):
        if any([self.data[f'answers-{i}-text'] == '' for i in range(3)]):
            raise forms.ValidationError('У вопроса должно быть 3 ответа')

        answers_correct_state = [self.data.get(f'answers-{i}-is_correct') for i in range(3)]
        if all([i is None for i in answers_correct_state]) or len(
                list(filter(lambda x: x is not None, answers_correct_state))) > 1:
            raise forms.ValidationError('Необходимо выбрать 1 правильный ответ')


class AnswerInline(NestedStackedInline):
    model = Answer
    fields = ('text', 'is_correct', 'order',)
    extra = 3
    max_num = 3


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    save_on_top = True
    form = QuestionAdminForm
    list_display = ('label', 'order', 'is_active',)
    inlines = [
        AnswerInline,
    ]
