from django.contrib import admin

from ..forms import QuestionnaireForm


@admin.register(QuestionnaireForm)
class QuestionAdmin(admin.ModelAdmin):
    pass
