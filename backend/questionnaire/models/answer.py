from django.db import models


class Answer(models.Model):
    question = models.ForeignKey(
        'Question', related_name="answers", on_delete=models.CASCADE, verbose_name="Вопрос")
    text = models.CharField(max_length=1024, verbose_name="Текст ответа")
    is_correct = models.BooleanField(
        default=False, verbose_name="Это правильный ответ?")
    order = models.IntegerField(
        default=1, verbose_name="Порядковый номер ответа")

    def __str__(self):
        return self.text

    class Meta:
        verbose_name_plural = "Ответы к вопросу"
        ordering = ("order",)