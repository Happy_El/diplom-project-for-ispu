from django.db import models
from user.models import User

from ..models import Answer


class Question(models.Model):
    label = models.TextField(verbose_name="Текст вопроса")
    order = models.IntegerField(default=1, verbose_name="Сортироква")
    page = models.ForeignKey(
        'additional.AdditionalPage', related_name="page", on_delete=models.CASCADE, verbose_name="Страница")
    passed_wrong = models.ManyToManyField(
        User, blank=True, related_name="passed_wrong", verbose_name="Неправильно ответившие пользователи")
    passed_correct = models.ManyToManyField(
        User, blank=True, related_name="passed_correct", verbose_name="Правильно ответившие пользователи")

    is_active = models.BooleanField(default=True, verbose_name="Отображается пользователям")

    def check_answer(self, answer):
        if answer in self.answers.filter(is_correct=True):
            return True
        else:
            return False

    def __str__(self):
        return self.label

    class Meta:
        ordering = ("order",)
        verbose_name = "Вопрос"
        verbose_name_plural = "Вопросы"
