from .additional_page import AdditionalPageAdmin  # noqa
from .registration_page import RegistrationPageAdmin  # noqa
from .login_page import LoginPageAdmin  # noqa
from .search_page import SearchPageAdmin  # noqa
from .personal_area_page import PersonalAreaPageAdmin  # noqa
