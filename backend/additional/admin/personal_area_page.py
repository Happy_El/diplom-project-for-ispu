from ..models.personal_area_page import PersonalAreaPage
from django.contrib import admin
from garpix_page.admin import BasePageAdmin


@admin.register(PersonalAreaPage)
class PersonalAreaPageAdmin(BasePageAdmin):
    pass
