from ..models.registration_page import RegistrationPage
from django.contrib import admin
from garpix_page.admin import BasePageAdmin


@admin.register(RegistrationPage)
class RegistrationPageAdmin(BasePageAdmin):
    pass
