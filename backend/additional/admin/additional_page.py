from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from django.contrib import admin


from questionnaire.models import Question
from questionnaire.admin import AnswerInline
from ..models.additional_page import AdditionalPage


class QuestionInline(NestedStackedInline):
    model = Question
    inlines = [AnswerInline]


@admin.register(AdditionalPage)
class AdditionalPageAdmin(NestedModelAdmin):
    inlines = [QuestionInline, ]
