from .additional_page import AdditionalPageTranslationOptions  # noqa
from .registration_page import RegistrationPageTranslationOptions  # noqa
from .login_page import LoginPageTranslationOptions  # noqa
from .search_page import SearchPageTranslationOptions  # noqa
from .personal_area_page import PersonalAreaPageTranslationOptions  # noqa
