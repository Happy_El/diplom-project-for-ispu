from modeltranslation.translator import TranslationOptions, register
from ..models import AdditionalPage


@register(AdditionalPage)
class AdditionalPageTranslationOptions(TranslationOptions):
    pass
