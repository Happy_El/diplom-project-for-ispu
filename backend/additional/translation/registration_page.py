from modeltranslation.translator import TranslationOptions, register
from ..models import RegistrationPage


@register(RegistrationPage)
class RegistrationPageTranslationOptions(TranslationOptions):
    pass
