from modeltranslation.translator import TranslationOptions, register
from ..models import PersonalAreaPage


@register(PersonalAreaPage)
class PersonalAreaPageTranslationOptions(TranslationOptions):
    pass
