from .additional_page import AdditionalPage  # noqa
from .registration_page import RegistrationPage  # noqa
from .login_page import LoginPage  # noqa
from .search_page import SearchPage  # noqa
from .personal_area_page import PersonalAreaPage  # noqa
