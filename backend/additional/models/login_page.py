from django.db import models
from garpix_page.models import BasePage

from user.views.login import LoginForm
from user.views.login import user_login


class LoginPage(BasePage):
    pass
    template = "pages/login.html"

    class Meta:
        verbose_name = "Авторизация"
        verbose_name_plural = "Авторизация"
        ordering = ("-created_at",)

    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        form = LoginForm()
        error = []
        if request.method == "POST":
            print(request.POST)
            try:
                form = user_login(request)[0]
                error = user_login(request)[1]
            except:
                form = '/'
                error = None

        context.update(
            {
                'form': form,
                'error': error
            }
        )
        return context
