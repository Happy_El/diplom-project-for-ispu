from django.db import models
from garpix_page.models import BasePage


class PersonalAreaPage(BasePage):
    template = "pages/personal_area.html"

    class Meta:
        verbose_name = "Личный кабинет"
        verbose_name_plural = "Личный кабинет"
        ordering = ("-created_at",)

    # def get_context(self, request=None, *args, **kwargs):
    #     context = super().get_context(request, *args, **kwargs)
    #
    #     context.update({
    #
    #     })
    #
    #     return context
