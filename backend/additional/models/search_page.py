from django.db import models
from django.db.models import Q
from garpix_page.models import BaseListPage
from functools import reduce

from additional.models import AdditionalPage


class SearchPage(BaseListPage):
    paginate_by = 25
    template = 'pages/search.html'

    class Meta:
        verbose_name = "Search"
        verbose_name_plural = "Searchs"
        ordering = ('-created_at',)

    def get_context(self, request=None, *args, **kwargs):
        querry_string = request.GET['q']
        search_result = []
        models_to_search = [AdditionalPage]

        querry_words = querry_string.split(' ')
        transdict = {'q': 'й', 'w': 'ц', 'e': 'у', 'r': 'к', 't': 'е', 'y': 'н', 'u': 'г', 'i': 'ш', 'o': 'щ', 'p': 'з',
                     '[': 'х',
                     ']': 'ъ', 'a': 'ф', 's': 'ы', 'd': 'в', 'f': 'а', 'g': 'п', 'h': 'р', 'j': 'о', 'k': 'л', 'l': 'д',
                     ';': 'ж',
                     '"': 'э', 'z': 'я', 'x': 'ч', 'c': 'с', 'v': 'м', 'b': 'и', 'n': 'т', 'm': 'ь', ',': 'б', '.': 'ю',
                     '`': 'ё'}
        trantab = str.maketrans(''.join(transdict.keys()), ''.join(transdict.values()))
        if querry_string == '':
            return {
                'objects': [],
            }

        counter = 0
        while counter < 2:
            for model in models_to_search:
                tmp1 = model.objects.filter(
                    reduce(lambda q1, q2: q1 & q2, [Q(title__icontains=word) for word in querry_words], Q()))
                tmp2 = model.objects.filter(  # noqa
                    reduce(lambda q1, q2: q1 & q2, [Q(content__icontains=word) for word in querry_words], Q()))
                tmp = tmp1
                if tmp:
                    search_result.append(tmp)

            if len(search_result) == 0:
                querry_words = [i.translate(trantab) for i in querry_words]
                counter += 1
            else:
                counter = 2
        return {
            'objects': search_result,
        }