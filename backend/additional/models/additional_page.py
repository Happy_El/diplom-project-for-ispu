from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from garpix_page.models import BasePage

from questionnaire.models import Question


class AdditionalPage(BasePage):
    template = "pages/additional.html"
    content = RichTextUploadingField(verbose_name='Содержание', blank=True, default='')

    class Meta:
        verbose_name = "Дополнительная страница"
        verbose_name_plural = "Дополнительные страницы"
        ordering = ("-created_at",)

    def __str__(self):
        return f'{self.title}'

    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        questions = Question.objects.filter(page=self)
        right_answers = []
        wrong_answers = []
        if request.method == 'POST':
            right = []
            answers = list(request.POST.items())[1:]
            for question in questions:
                right.append(question.answers.get(is_correct=True))
            for i in range(len(answers)):
                if str(answers[i][1]) == str(right[i]):
                    right_answers.append([str(questions[i]), str(answers[i][1])])
                    if request.user.is_authenticated:
                        questions[i].passed_correct.add(request.user)
                        questions[i].save()
                else:
                    wrong_answers.append([str(questions[i]), str(answers[i][1]), str(right[i])])
                    if request.user.is_authenticated:
                        questions[i].passed_wrong.add(request.user)
                        questions[i].save()

        objects = AdditionalPage.objects.all()
        slug = request.path.split('/')[-1]
        object = objects.get(slug=slug)
        objects.order_by('created_at')
        prev_obj_url = None
        next_obj_url = None
        if object != list(objects)[0]:
            prev_obj_url = list(objects)[list(objects).index(object) - 1]
        if object != list(objects)[-1]:
            next_obj_url = list(objects)[list(objects).index(object) + 1]
        context.update(
            {
                'questions': questions,
                'prev_obj_url': prev_obj_url,
                'next_obj_url': next_obj_url,
                'right_answers': right_answers,
                'wrong_answers': wrong_answers,
            }
        )
        return context
