from django.db import models
from django.shortcuts import redirect
from garpix_page.models import BasePage
from user.forms.registration import registration_form_handler

from user.forms import RegistrationForm


class RegistrationPage(BasePage):
    template = "pages/registration.html"

    class Meta:
        verbose_name = "Страница регистрации"
        verbose_name_plural = "Страницы регистрации"
        ordering = ("-created_at",)

    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        form = RegistrationForm()
        error = []
        if request.method == "POST":
            print(request.POST)
            try:
                form = registration_form_handler(request)[0]
                error = registration_form_handler(request)[1]
            except:
                form = '/'
                error = None

        context.update(
            {
                'form': form,
                'error': error
            }
        )
        return context
