from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AdditionalConfig(AppConfig):
    name = 'additional'
    verbose_name = _('Additional')
