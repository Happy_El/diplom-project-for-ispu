# Generated by Django 3.1 on 2022-04-28 15:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_remove_homepage_pages'),
        ('additional', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='additionalpage',
            name='pages',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='home.homepage', verbose_name='Страница (привязка)'),
        ),
    ]
