from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import login


from ..models import User


class RegistrationForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['email'].label = ""
        self.fields['first_name'].label = ""
        self.fields['last_name'].label = ""
        self.fields['username'].label = ""
        self.fields['password1'].label = ""
        self.fields['password2'].label = ""

    first_name = forms.CharField(label='Имя', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Имя', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))
    last_name = forms.CharField(label='Фамилия', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Фамилия', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))
    username = forms.CharField(label='Логин', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Юзернейм', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))
    email = forms.EmailField(label='e-mail', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'E-mail', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))
    password1 = forms.CharField(label='Пароль', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Пароль', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))
    password2 = forms.CharField(label='Повторение пароля', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Повторите пароль', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))


def registration_form_handler(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            password1 = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            error = []

            try:
                user_mail = User.objects.get(email=email)
            except:
                user_mail = None
            try:
                user_uname = User.objects.get(username=username)
            except:
                user_uname = None

            if user_mail:
                error.append('Пользователь с такой почтой уже есть')
            if user_uname:
                error.append('Пользователь с таким юзернеймом уже существует')
            if password1 != password2:
                error.append('Пароли не совпадают')

            if error:
                return form, error
            else:
                user = User(
                    email=email,
                    username=username,
                    first_name=first_name,
                    last_name=last_name,
                )
                user.set_password(password1)
                user.save()

                login(request, user)
        else:
            print('form is wrong')
            return form
