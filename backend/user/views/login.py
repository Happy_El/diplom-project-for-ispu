from django.contrib.auth import login, authenticate
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django import forms


from user.serializers.login import LoginSerializer  # noqa
from user.models import User


class LoginForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.fields['username'].label = ""
        self.fields['password'].label = ""
    username = forms.CharField(label='Имя', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Логин', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))
    password = forms.CharField(label='Пароль', max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Пароль', 'style': 'width: 300px; margin: 1%;', 'class': 'form-control'}))


def user_login(request):
    print(request.POST)
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return '/'
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')

