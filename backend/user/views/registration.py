from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import authentication
from rest_framework.generics import CreateAPIView
from django.contrib.auth import get_user_model

from ..serializers import UserRegisterSerializer


class CsrfExemptSessionAuthentication(authentication.SessionAuthentication):
    def enforce_csrf(self, request):
        return


class UserCreateView(CreateAPIView):
    model = get_user_model()
    serializer_class = UserRegisterSerializer
    authentication_classes = [CsrfExemptSessionAuthentication]
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def post(self, request, *args, **kwargs):
    #     promo_config = PromoConfiguration.get_solo()
    #
    #     if not promo_config.is_started:
    #         return Response({'status': 'not started'}, status=status.HTTP_400_BAD_REQUEST)
    #
    #     elif promo_config.is_over:
    #         return Response({'status': 'is over'}, status=status.HTTP_400_BAD_REQUEST)
    #
    #     return self.create(request, *args, **kwargs)