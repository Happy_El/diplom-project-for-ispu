from django.contrib.auth import authenticate
from rest_framework import serializers

from ..models import User


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        try:
            _user = User.objects.get(username=attrs['username'].lower())
        except Exception:
            raise serializers.ValidationError({'user_not_found': ''})

        if not _user.is_active:
            raise serializers.ValidationError({'not_active': 'Ваш профиль пока не прошел модерацию.'})

        user = authenticate(username=attrs['username'].lower(), password=attrs['password'])

        if not user:
            raise serializers.ValidationError({'password': 'Неверный пароль.'})

        return {'user': user}