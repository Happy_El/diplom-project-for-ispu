from rest_framework import serializers
import django.contrib.auth.password_validation as validators
from django.core import exceptions
from garpix_notify.models import Notify
from django.conf import settings
from datetime import datetime
import re
import logging

from ..models import User

logger = logging.getLogger('django')
email_pattern = re.compile('[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}')  # noqa


class UserRegisterSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    # full_name = serializers.CharField(max_length=100)
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    email = serializers.CharField(max_length=100)

    consent = serializers.BooleanField()

    # birthday = serializers.DateField(input_formats=['%d-%m-%Y'], source='birthday')

    class Meta:
        model = User
        fields = ["email", "password1", "password2", "consent"]
        # read_only_fields = ['password']
        extra_kwargs = {
            'email': {'required': True},
            'full_name': {'required': True},
            'password1': {'required': True},
            'password2': {'required': True},
        }

    # def validate_full_name(self, value):
    #     if len(value) > 70:
    #         raise serializers.ValidationError("ФИО не может быть длиннее 70 символов.")
    #     return value

    def validate_email(self, value):
        if not email_pattern.fullmatch(value):
            raise serializers.ValidationError("Введите корректный E-mail.")
        return value

    def validate_consent(self, value):
        if not value:
            raise serializers.ValidationError("Необходимо cогласие с политикой конфиденциальности")
        return value

    def validate_password1(self, value):
        try:
            validators.validate_password(password=value, user=User)
            return value
        except exceptions.ValidationError as e:
            logger.error("ERROR", e, '<br>'.join(e))
            raise serializers.ValidationError('<br>'.join(e))

    def save(self):
        user = User(
            email=self.validated_data['email'].lower(),
            username=self.validated_data['email'].lower(),
            first_name=self.validated_data['first_name'].lower(),
            last_name=self.validated_data['last_name'].lower(),
        )
        pass1 = self.validated_data['password1']
        pass2 = self.validated_data['password2']
        if pass1 != pass2:
            raise serializers.ValidationError({'password2': 'Пароли не совпадают'})
        user.set_password(pass1)
        user.save()

        # Notify.send(
        #     settings.NOTIFY_REGISTRATION_EVENT,
        #     {'email': user.email, 'full_name': user.full_name, 'link': settings.SITE_URL + '/promo/#login'},
        #     email=user.email)

        # config = SiteConfiguration.get_solo()
        # уведомление админу о регистрации участника, емейл захардкодил
        # Notify.send(
        #     settings.NOTIFY_REGISTRATION_ADMIN_EVENT,
        #     {'email': user.email, 'full_name': user.full_name,
        #      'link': settings.SITE_URL + '/promo/#login'},
        #     email=config.email_common)

        return user
