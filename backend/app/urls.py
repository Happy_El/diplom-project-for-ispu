from django.conf.urls import url
from garpixcms.urls import *  # noqa



urlpatterns = [
                  path(r'logout', LogoutView.as_view(url='/'), name='logout'),
              ] + urlpatterns  # noqa
