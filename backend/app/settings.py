import os
from .basedir import BASE_DIR
from garpixcms.settings import *  # noqa


INSTALLED_APPS += [  # noqa
    'home',
    'additional',
    'questionnaire',

]


INSTALLED_APPS += [ # noqa
    'sass_processor',
    'nested_inline'
]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, '..', 'frontend', 'static'),
]

STATIC_ROOT = BASE_DIR / 'static'

SASS_PROCESSOR_ROOT = os.path.join(BASE_DIR, '..', 'frontend', 'static')

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'sass_processor.finders.CssFinder',
]

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)

MEDIA_ROOT = os.path.join(BASE_DIR, '..', 'public', 'media')
MEDIA_URL = '/media/'
