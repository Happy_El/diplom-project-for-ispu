from garpix_menu.mixins import LinkMixin
from django.db import models


class CarouselBanner(LinkMixin):
    title = models.CharField(max_length=100, verbose_name='Заголовок')
    page = models.ForeignKey('HomePage', on_delete=models.CASCADE, verbose_name='Страница (привязка)', related_name='carousel_items')
    image = models.ImageField(verbose_name='Изображение')
    banner_text = models.CharField(max_length=30, verbose_name='Текст банера1', blank=True, null=True)
    is_active = models.BooleanField(default=False, verbose_name='Показывать?')
    sort = models.PositiveSmallIntegerField(default=100, verbose_name='Сортировка')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Элемент карусели'
        verbose_name_plural = 'Элементы карусели'
        ordering = ('sort',)