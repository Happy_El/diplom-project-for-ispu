from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from garpix_page.models import BasePage
from additional.models import AdditionalPage


class HomePage(BasePage):
    template = "pages/home.html"
    short_description = RichTextUploadingField(verbose_name='Содержание', blank=True, default='')

    class Meta:
        verbose_name = "Домашняя страница"
        verbose_name_plural = "Домашние страницы"
        ordering = ("-created_at",)

    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        short_description = self.short_description
        pages = AdditionalPage.objects.all()
        context.update(
            {
                'short_description': short_description,
                'pages': pages,
            }
        )
        return context
