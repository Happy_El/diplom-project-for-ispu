# Generated by Django 3.1 on 2022-04-28 15:29

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('additional', '0001_initial'),
        ('garpix_page', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomePage',
            fields=[
                ('basepage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='garpix_page.basepage')),
                ('short_description', ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='', verbose_name='Содержание')),
                ('pages', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='additional.additionalpage', verbose_name='Страница (привязка)')),
            ],
            options={
                'verbose_name': 'Домашняя страница',
                'verbose_name_plural': 'Домашние страницы',
                'ordering': ('-created_at',),
            },
            bases=('garpix_page.basepage',),
        ),
        migrations.CreateModel(
            name='CarouselBanner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(blank=True, max_length=1000, null=True, verbose_name='Внешний URL')),
                ('hash', models.CharField(blank=True, default='', help_text='Если хотите дать ссылку на конкретный элемент страницы. Например - #example', max_length=256, verbose_name='Якорь')),
                ('title', models.CharField(max_length=100, verbose_name='Заголовок')),
                ('image', models.ImageField(upload_to='', verbose_name='Изображение')),
                ('banner_text', models.CharField(blank=True, max_length=30, null=True, verbose_name='Текст банера1')),
                ('is_active', models.BooleanField(default=False, verbose_name='Показывать?')),
                ('sort', models.PositiveSmallIntegerField(default=100, verbose_name='Сортировка')),
                ('page', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='carousel_items', to='home.homepage', verbose_name='Страница (привязка)')),
            ],
            options={
                'verbose_name': 'Элемент карусели',
                'verbose_name_plural': 'Элементы карусели',
                'ordering': ('sort',),
            },
        ),
    ]
