from ..models.home_page import HomePage
from django.contrib import admin
from garpix_page.admin import BasePageAdmin

from ..models import CarouselBanner


class CarouselBannerInline(admin.TabularInline):
    model = CarouselBanner
    fk_name = 'page'
    extra = 1


@admin.register(HomePage)
class HomePageAdmin(BasePageAdmin):
    inlines = (CarouselBannerInline, )

